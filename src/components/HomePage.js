import React, { useState, useContext, useEffect } from "react";
import { SharedState } from "../App";
import { Title, Artist, Album } from "./TextBits";
import {
  PlayButton,
  StopButton,
  VolumeDownIcon,
  VolumeUpIcon,
  BuyButton,
  SettingsButton,
  BufferingIcon,
  Bandcamp,
  DiscordButton,
  FacebookButton,
} from "./Buttons";
import { MobileViewHome, Cont, ShowInfo } from "./MobileView";
import {
  BrowserView,
  isMobile,
  ConsoleView,
  TabletView,
  MobileOnlyView,
} from "react-device-detect";

export function HomePage(props) {
  const { metadata } = useContext(SharedState);
  const { showInfo } = useContext(SharedState);
  const { isLive } = metadata;
  return (
    <div
      className={`homegrid ${isLive ? `live` : null} ${
        isMobile ? `mobile` : null
      }`}
    >
      <BrowserView renderWithFragment>
        <div className="logo" />
        <Nav />
        <div
          className="art"
          style={{
            backgroundImage: `url(${
              metadata.covers
                ? metadata.covers[512]
                : "https://res.cloudinary.com/stormdragon-designs/image/upload/v1526676139/fricon_600-600_zersgq.png"
            })`,
            backgroundSize: "contain",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
          }}
        >
          {/* <AlbumArt size={512} /> */}
        </div>
        <NowPlayingDesktop />
        <PreviouslyPlayed />
        <Controls />
        {isLive ? <LivePanel /> : null}
      </BrowserView>
      <ConsoleView renderWithFragment>
        <div className="logo" />
        <Nav />
        <div
          className="art"
          style={{
            backgroundImage: `url(${
              metadata.covers
                ? metadata.covers[512]
                : "https://res.cloudinary.com/stormdragon-designs/image/upload/v1526676139/fricon_600-600_zersgq.png"
            })`,
            backgroundSize: "contain",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
          }}
        >
          {/* <AlbumArt size={512} /> */}
        </div>
        <NowPlayingDesktop />
        <PreviouslyPlayed />
        <Controls />
        {isLive ? <LivePanel /> : null}
      </ConsoleView>
      <MobileOnlyView renderWithFragment>
        <MobileViewHome />
      </MobileOnlyView>
      <TabletView renderWithFragment>
        <div className="logo" />
        <div className="art">
          <AlbumArt size={512} />
        </div>
        <NowPlayingDesktop />
        <PreviouslyPlayed />
        <Controls />
        <Cont />
        {showInfo ? <ShowInfo /> : null}
      </TabletView>
    </div>
  );
}

const Nav = (props) => {
  return (
    <div className="nav">
      <ul>
        <li>
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://fillyradio.com/discord"
          >
            <DiscordButton />
          </a>
        </li>
        <li>
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://www.facebook.com/FillydelphiaRadio/"
          >
            <FacebookButton />
          </a>
        </li>
      </ul>
    </div>
  );
};

const LivePanel = (props) => {
  const { metadata } = useContext(SharedState);
  return (
    // {
    //   metadata.icename ?
    //   metadata.icename ?
    //   metadata.icename ?
    <div className="banner">
      <p className="djname">
        Live On Air: {metadata.icename ? metadata.icename : "Live DJ"}
      </p>
    </div>
    // }
  );
};

export const AlbumArt = (props) => {
  const { metadata } = useContext(SharedState);
  const covers = metadata.covers;

  return (
    <img
      alt="Album Artwork"
      src={
        covers
          ? covers[props.size]
          : "https://res.cloudinary.com/stormdragon-designs/image/upload/c_scale,w_" +
            props.size +
            "/v1526676139/fricon_600-600_zersgq.png"
      }
    />
  );
};

function Controls(props) {
  return (
    <div className="control">
      <PlaybackControl />
      <VolumeSlider />
      <More number={0} />
      <Settings />
    </div>
  );
}

export function Settings() {
  const [settingsMenu, setSettingsMenu] = useState(false);
  const handleSettingsClick = () => {
    setSettingsMenu(!settingsMenu);
  };
  return (
    <div className="dropdown">
      <SettingsButton action={handleSettingsClick} />
      {settingsMenu ? <SettingsDropUp blur={setSettingsMenu} /> : null}
    </div>
  );
}

export function More(props) {
  const [moreMenu, setMoreMenu] = useState(false);
  const [buyUrl, setBuyUrl] = useState("null");
  const [store, setStore] = useState("loading");
  const num = props.number;
  const { metadata } = useContext(SharedState);
  const { history } = metadata;

  const handleMoreClick = () => {
    setMoreMenu(!moreMenu);
  };

  useEffect(() => {
    if (history) {
      if (history[num]) {
        if (history[num].store) {
          if (history[num].store !== "none") {
            setBuyUrl(history[num].wwwpublisher);
            setStore(history[num].store);
          } else {
            setStore(false);
          }
        }
      }
    }
  }, [history, num]);

  return (
    <div className="dropdown" style={{ marginLeft: "auto" }}>
      <BuyButton size={props.size} action={handleMoreClick} />
      {moreMenu ? (
        <MoreDropUp
          buyUrl={buyUrl}
          store={store}
          number={props.number}
          blur={setMoreMenu}
        />
      ) : null}
    </div>
  );
}

function MoreDropUp(props) {
  const { store, buyUrl } = props;

  let timeOutId = null;

  let menuBox = React.createRef();

  useEffect(() => {
    menuBox.current.focus();
  });

  const onBlurHandler = () => {
    timeOutId = setTimeout(() => {
      props.blur(false);
    });
  };

  const onFocusHandler = () => {
    clearTimeout(timeOutId);
  };

  return (
    <div
      className={`dropdown-content show`}
      tabIndex="0"
      ref={menuBox}
      onBlur={onBlurHandler}
      onFocus={onFocusHandler}
      style={props.size ? { bottom: 0 + "px" } : null}
    >
      <ul onFocus={onFocusHandler}>
        {buyUrl && store ? (
          <a
            href={buyUrl}
            onFocus={onFocusHandler}
            target="_blank"
            rel="noopener noreferrer"
          >
            <li style={{ whiteSpace: "nowrap" }}>
              {store === "Bandcamp" ? <Bandcamp size={1.4} /> : null} {store}
            </li>
          </a>
        ) : (
          <li style={{ whiteSpace: "nowrap" }}>
            <em>No purchase link ☹</em>
          </li>
        )}
      </ul>
    </div>
  );
}

/**
 * Shows the Volume Control and.. uh... controls it...
 * @param {props} props
 */
function VolumeSlider(props) {
  const { volume, setVolumeHandler } = useContext(SharedState);
  const handleVolumeChange = (e) => {
    setVolumeHandler(e.target.value);
  };
  const handleVolumeClick = (e) => {
    if (e === 0) {
      if (volume > 10) {
        setVolumeHandler(volume - 10);
      } else setVolumeHandler(0);
    } else if (e === 1) {
      if (volume < 90) {
        setVolumeHandler(volume + 10);
      } else setVolumeHandler(100);
    }
  };
  return (
    <div>
      <VolumeDownIcon size={24} action={() => handleVolumeClick(0)} />
      <input
        type="range"
        value={volume}
        onChange={handleVolumeChange}
        onMouseUp={handleVolumeChange}
        min="0"
        max="100"
        step="1"
      />
      &nbsp;
      <VolumeUpIcon size={24} action={() => handleVolumeClick(1)} />
    </div>
  );
}

/**
 * Shows the Playback Control. Play, Buffering, Pause... that thing
 * @param {props} props
 */
export function PlaybackControl(props) {
  const {
    audioStatus,
    setAudioStatus,
    playStatus,
    setPlayStatus,
    metadata,
  } = useContext(SharedState);
  const togglePlayStatus = () => {
    if (playStatus === "PLAYING") {
      setPlayStatus("STOPPED");
    } else if (playStatus === "STOPPED" && metadata.title) {
      setPlayStatus("PLAYING");
      setAudioStatus("Buffering");
    }
  };
  return (
    <React.Fragment>
      {audioStatus === "Stopped" &&
      playStatus !== "PLAYING" &&
      (metadata.title || metadata.artist) ? (
        <PlayButton action={() => togglePlayStatus()} />
      ) : null}
      {audioStatus === "Playing" ? (
        <StopButton action={() => togglePlayStatus()} />
      ) : null}
      {audioStatus === "Buffering" || (!metadata.title && !metadata.artist) ? (
        <BufferingIcon />
      ) : null}
      {audioStatus === "Stopped" && playStatus === "PLAYING" ? (
        <BufferingIcon />
      ) : null}
      {audioStatus === "iOS" ? (
        <React.Fragment>
          <PlayButton />
        </React.Fragment>
      ) : null}
    </React.Fragment>
  );
}

/**
 * The panel that shows when you click the Settings button in `.controls`. Has links to the M3U files, and sets the player quality.
 * @param {props} props
 */
function SettingsDropUp(props) {
  const { streamUrl, setStreamUrlHandler } = useContext(SharedState);
  var timeOutId = null;
  const style = { border: "black solid 2px" };
  const setQuality = (q) => {
    if (q === 64) {
      setStreamUrlHandler("https://fillyradio.com/stream-64k");
    } else if (q === 128) {
      setStreamUrlHandler("https://fillyradio.com/stream-128k");
    } else if (q === 320) {
      setStreamUrlHandler("https://fillyradio.com/stream-320k");
    } else if (q === 8) {
      setStreamUrlHandler("https://fillyradio.com/stream-hqogg");
    }
  };
  let menuBox = React.createRef();

  useEffect(() => {
    menuBox.current.focus();
  });

  const onBlurHandler = () => {
    timeOutId = setTimeout(() => {
      props.blur(false);
    });
  };

  const onFocusHandler = () => {
    clearTimeout(timeOutId);
  };

  return (
    <div
      className={`dropdown-content show`}
      tabIndex="0"
      ref={menuBox}
      onBlur={onBlurHandler}
      onFocus={onFocusHandler}
    >
      <p>Quality</p>
      <hr />
      <div
        style={streamUrl === "https://fillyradio.com/stream-64k" ? style : null}
        onClick={() => setQuality(64)}
        onFocus={onFocusHandler}
      >
        <p>AAC+ 64k</p>
      </div>
      <div
        style={
          streamUrl === "https://fillyradio.com/stream-128k" ? style : null
        }
        onClick={() => setQuality(128)}
        onFocus={onFocusHandler}
      >
        <p>MP3 128k</p>
      </div>
      <div
        style={
          streamUrl === "https://fillyradio.com/stream-320k" ? style : null
        }
        onClick={() => setQuality(320)}
        onFocus={onFocusHandler}
      >
        <p>MP3 320k</p>
      </div>
      <div
        style={
          streamUrl === "https://fillyradio.com/stream-hqogg" ? style : null
        }
        onClick={() => setQuality(8)}
        onFocus={onFocusHandler}
      >
        <p>HQ Ogg Vorbis</p>
      </div>
      <hr />
      <p>External Player</p>
      <hr />
      <div onFocus={onFocusHandler}>
        <a
          target="_blank"
          rel="noopener noreferrer"
          download
          href="https://firebasestorage.googleapis.com/v0/b/fillydelphia-radio.appspot.com/o/Fillydelphia-Radio-AAC-64k.m3u?alt=media&token=d9d4b5e1-92f9-4eff-943b-ef9b899e7236"
        >
          64k AAC
        </a>
      </div>
      <div onFocus={onFocusHandler}>
        <a
          target="_blank"
          rel="noopener noreferrer"
          download
          href="https://firebasestorage.googleapis.com/v0/b/fillydelphia-radio.appspot.com/o/Fillydelphia-Radio-MP3-128k.m3u?alt=media&token=f668a081-a3cb-4a65-8cb7-9407135fe08b"
        >
          128k MP3
        </a>
      </div>
      <div onFocus={onFocusHandler}>
        <a
          target="_blank"
          rel="noopener noreferrer"
          download
          href="https://firebasestorage.googleapis.com/v0/b/fillydelphia-radio.appspot.com/o/Fillydelphia-Radio-MP3-320k.m3u?alt=media&token=225f03ea-7d64-4a5d-b748-f5c35f94258f"
        >
          320k MP3
        </a>
      </div>
    </div>
  );
}

export function NowPlayingDesktop(props) {
  const { metadata } = useContext(SharedState);
  const sizes = getOptimaltextSize(metadata);
  if (metadata.title) {
    return (
      <div className={`nowplayingtext`}>
        <Title size={sizes.titleSize} />
        <Artist pre="by " size={sizes.artistSize} />
        <Album pre="from " size={sizes.albumSize} />
      </div>
    );
  } else {
    return (
      <div className={`nowplayingtext`}>
        <div />
        <div style={{ textAlign: "center", fontSize: "4vh" }}>
          <BufferingIcon />
        </div>
        <div />
      </div>
    );
  }
}

function PreviouslyPlayedSingle(props) {
  const { metadata } = useContext(SharedState);
  const { history } = metadata;
  const style = {
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
    margin: "0.5vh",
    fontSize: "1.5vh",
  };
  let minutesSince = 0;
  if (history[props.number].on_air) {
    minutesSince = CalculateTimeSince(history, props, minutesSince);
  }

  return (
    <React.Fragment>
      <div className={`played-${props.number}`}>
        <img src={history[props.number].covers[96]} alt="Past Cover 1" />

        <div className="text">
          <p style={style}>{history[props.number].title}</p>
          <p style={style}>{history[props.number].artist}</p>
          <p style={style}>{history[props.number].album}</p>
          {minutesSince ? (
            <p style={style}>Played {minutesSince} minutes ago</p>
          ) : null}
        </div>
      </div>
      <div className={`option-${props.number}`}>
        <More size={32} number={props.number} />
      </div>
    </React.Fragment>
  );
}

function CalculateTimeSince(history, props, minutesSince) {
  const timePlayed = new Date(history[props.number].on_air + " GMT+0000");
  const timeSince = Date.now() - new Date(timePlayed);
  minutesSince = Math.round(timeSince / 60000);
  return minutesSince;
}

function PreviouslyPlayed(props) {
  const { metadata } = useContext(SharedState);
  const { history } = metadata;
  return history ? (
    <React.Fragment>
      {history[1] ? <PreviouslyPlayedSingle number={1} /> : null}
      {history[2] ? <PreviouslyPlayedSingle number={2} /> : null}
      {history[3] ? <PreviouslyPlayedSingle number={3} /> : null}
      {history[4] ? <PreviouslyPlayedSingle number={4} /> : null}
    </React.Fragment>
  ) : null;
}

function getOptimaltextSize(metadata) {
  let titleSize = 4,
    albumSize = 4,
    artistSize = 4;
  if (metadata.title) {
    if (metadata.title.length > 35) {
      titleSize = 4.5;
    } else if (metadata.title.length > 20) {
      titleSize = 5.5;
    } else if (metadata.title.length > 16) {
      titleSize = 6;
    } else if (metadata.title.length > 13) {
      titleSize = 7;
    } else {
      titleSize = 8;
    }
  }
  if (metadata.artist) {
    if (metadata.artist.length > 35) {
      artistSize = 3.5;
    } else if (metadata.artist.length > 20) {
      artistSize = 4;
    } else if (metadata.artist.length > 16) {
      artistSize = 4.5;
    } else if (metadata.artist.length > 13) {
      artistSize = 5;
    } else {
      artistSize = 5.5;
    }
  }

  if (metadata.album) {
    if (metadata.album.length > 35) {
      albumSize = 2.5;
    } else if (metadata.album.length > 20) {
      albumSize = 3;
    } else if (metadata.album.length > 16) {
      albumSize = 3.5;
    } else if (metadata.album.length > 13) {
      albumSize = 4;
    } else {
      albumSize = 4.5;
    }
  }

  return { titleSize, albumSize, artistSize };
}
